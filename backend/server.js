const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const socketServer = require("socket.io");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var serve = http.createServer(app);
var io = socketServer(serve);
serve.listen(4000, () => {
  console.log("Server with socket running on port 4000!");
});

const connections = [];
io.on("connection", function(socket) {
  console.log("Connected to Socket!!" + socket.id);
  connections.push(socket);
  socket.on("disconnect", function() {
    console.log("Disconnected - " + socket.id);
  });

  socket.on("addItem", addData => {
    io.emit("itemAdded", addData);
    console.log("ADD NEW ITEM WORKED !!!");
  });
});
