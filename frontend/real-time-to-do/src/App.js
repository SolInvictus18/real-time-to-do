import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import TodoList from "./Components/TodoList/TodoList";

import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <MuiThemeProvider>
            <TodoList />
          </MuiThemeProvider>
        </Provider>
      </div>
    );
  }
}

export default App;
