import React from "react";

import { List, ListItem } from "material-ui/List";
import RaisedButton from "material-ui/RaisedButton";
import TextField from "material-ui/TextField";

import { connect } from "react-redux";
import { addItem, addItemSocketIO } from "./actions";

import io from "socket.io-client";

let socket;

class TodoList extends React.Component {
  constructor() {
    super();
    this.state = {
      todos: []
    };
    socket = io.connect("http://localhost:4000");

    socket.on("itemAdded", res => {
      this.props.dispatch(addItem(res));
    });
  }

  componentWillUnmount() {
    socket.disconnect();
    alert("Disconnecting Socket as component will unmount");
  }
  handleClick() {
    let state_todos = this.state.todos;
    state_todos.push(this.refs.newTodo.input.value);
    this.setState({
      todos: state_todos
    });
    this.props.dispatch(addItemSocketIO(socket, this.refs.newTodo.input.value));
    this.refs.newTodo.input.value = ""; //Will clean the input of the text field
  }
  render() {
    return (
      <div>
        <TextField
          hintText="Add new todo"
          floatingLabelText="Enter the new todo"
          ref="newTodo"
        />
        <RaisedButton
          label="Add new todo"
          primary={true}
          onClick={this.handleClick.bind(this)}
        />
        <List>
          {this.props.todos.map((todo, key) => {
            return <ListItem key={key} primaryText={todo} />;
          })}
        </List>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

export default connect(mapStateToProps)(TodoList);
