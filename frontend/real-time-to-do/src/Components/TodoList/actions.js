export const addItem = data => ({
  type: "ADD_ITEM",
  todo: data
});

export const addItemSocketIO = (socket, item) => {
  return dispatch => {
    socket.emit("addItem", item);
  };
};
