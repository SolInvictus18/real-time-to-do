const initialState = {
  todos: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_ITEM":
      let todos = state.todos;
      todos.push(action.todo);
      return {
        ...state,
        todos: [...todos]
      };
    default:
      return state;
  }
};

export default reducer;
